import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { HeroesService } from "../../services/heroes.service";



@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})

export class BuscadorComponent implements OnInit {

  heroes:any[] = [];


  constructor( private _activated: ActivatedRoute,
                private _heroesService: HeroesService)

               { }

  ngOnInit(): void {

    this._activated.params.subscribe( params => {
      this.heroes = this._heroesService.buscarHeroes(params['termino']);
      console.log(this.heroes);
    })


  }

}
